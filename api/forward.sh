#!/bin/bash

#port=$1
host=$1

#echo $port
#echo $host

base_port=29000
find_flag=0

count=$(netstat -ntpl | grep $base_port | wc -l)
#echo $count

while (( base_port < 29050 )) && (( find_flag != 1 ))
do
  if [ $count = '0' ]
  then
    find_flag=1
  else
    base_port=$(( base_port + 1 ))
    count=$(netstat -ntpl | grep $base_port | wc -l)
  fi
done

echo \{\"port\":\"$base_port\",\"host\":\"$host\"\}

# /usr/bin/screen -dmS "$base_port->$host" autossh -M 0 -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -N -L 0.0.0.0:$base_port:$host:22 alex-kvm
/usr/bin/screen -dmS "$host->$base_port" autossh -M 0 -N -L 0.0.0.0:$base_port:$host:22 alex-kvm
