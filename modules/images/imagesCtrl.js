'use strict';

angular.module('lxdAdmin.images', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/images', {
            title: 'Images',
            templateUrl: 'modules/images/images.html',
            controller: 'imagesListCtrl',
            resolve: {
                images: function (imagesService) {
                    return imagesService.getAll();
                }
            }
        })

        ;
    }])

    .controller('imagesListCtrl', function($scope, $http, $interval, imagesService, images, $uibModal) {

    $scope.images = images;

    $scope.delete = function (image) {
        // Create modal
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'modules/images/modalImageDel.html',
            controller: 'genericImageModalCtrl',
            size: 'sm',
            resolve: {
                image: function () {
                    return image;
                }
            }
        });

        // Handle modal answer
        modalInstance.result.then(function (image) {
            imagesService.delete(image.fingerprint).then(function(data) {
                var operationUrl = data.data.operation;
                var refreshInterval;
                        imagesService.isOperationFinished(operationUrl).then(function(data) {
                          imagesService.getAll().then(function(data) {
                            $scope.images = data;
                          });
                        });
            });
        }, function () {
            // Nothing, user canceled
        });
    };

  })

  .controller('genericImageModalCtrl', function ($scope, $routeParams, $filter, $location, $uibModalInstance,
                                                     image, imagesService) {
      $scope.image = image;

      $scope.ok = function () {
          $uibModalInstance.close($scope.image);
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  })

  ;
