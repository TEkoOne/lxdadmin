'use strict';

angular.module('lxdAdmin.containers', ['ngRoute'])
    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/containers', {
            title: 'Containers',
            templateUrl: 'modules/containers/containers.html',
            controller: 'containersListCtrl',
            resolve: {
                containers: function (containersService) {
                    return containersService.getAll();
                },
                profiles: function(profileService) {
                    return profileService.getAll();
                }
            }
        })
        .when('/container-create', {
            title: 'Containers',
            templateUrl: 'modules/containers/container-create.html',
            controller: 'containerCreateCtrl',
            resolve: {
                images: function (imagesService, $route) {
                    return imagesService.getAll();
                },
                profiles: function(profileService) {
                    return profileService.getAll();
                }
            }
        })
        .when('/container/:containerName', {
            title: 'Container',
            templateUrl: 'modules/containers/container.html',
            controller: 'containerViewCtrl',
            resolve: {
                containerState: function (containersService, $route) {
                    return containersService.getState($route.current.params.containerName)
                },
                container: function (containersService, $route) {
                    return containersService.getByName($route.current.params.containerName)
                }
            }
        })
        .when('/container-snapshots/:containerName', {
            title: 'Container',
            templateUrl: 'modules/containers/container-snapshots.html',
            controller: 'containerSnapshotCtrl',
            resolve: {
                snapshots: function (containersService, $route) {
                    return containersService.getSnapshots($route.current.params.containerName)
                },
                container: function(containersService, $route) {
                    return containersService.getByName($route.current.params.containerName)
                }
            }
        })
        // .when('/container-edit/:containerName', {
        //     title: 'Container',
        //     templateUrl: 'modules/containers/container-edit.html',
        //     controller: 'containerViewCtrl',
        //     resolve: {
        //         containerState: function (ContainerServices, $route) {
        //             return ContainerServices.getState($route.current.params.containerName)
        //         },
        //         container: function (ContainerServices, $route) {
        //             return ContainerServices.getByName($route.current.params.containerName)
        //         }
        //     }
        // })
        ;
    }])

  .controller('containersListCtrl', function($scope, $http, $interval, $uibModal, containersService, containers, profiles) {

  $scope.containers = containers;
  $scope.profiles = profiles;

  $scope.changeState = function (container, state) {
      containersService.changeState(container.name, state).then(function(data) {
          var operationUrl = data.data.operation;
                  for(var n=0; n<$scope.containers.length; n++) {
                      if ($scope.containers[n].name == container.name) {
                          $scope.containers[n].disabled = true;
                      }
                  }
                  containersService.isOperationFinished(operationUrl).then(function(data) {
                    if ((container.ephemeral == true) && (state == 'stop')) {
                      var index = $scope.containers.indexOf(container);
                      $scope.containers.splice(index, 1);
                    }
                    else {
                      containersService.getByName(container.name).then(function(data) {
                          for(var n=0; n<$scope.containers.length; n++) {
                              if ($scope.containers[n].name == container.name) {
                                  $scope.containers[n] = data.data.metadata;
                              }
                          }
                      });
                    }
                  });
      });
  }

  $scope.createImage = function(container) {
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/containers/modalImageCreate.html',
          controller: 'containerImageCreateModalCtrl',
          size: 'sm',
          resolve: {
              container: function () {
                  return container;
              }
          }
      });

      modalInstance.result.then(function (imageData) {
          containersService.createImage(imageData.name,imageData.container.name).then(function (data) {
              var operationUrl = data.data.operation;
          });
      }, function () {
          // Nothing
      });
  }

  $scope.delete = function (container) {
      // Create modal
      var modalInstance = $uibModal.open({
          animation: $scope.animationsEnabled,
          templateUrl: 'modules/containers/modalContainerDel.html',
          controller: 'genericContainerModalCtrl',
          size: 'sm',
          resolve: {
              container: function () {
                  return container;
              }
          }
      });

      // Handle modal answer
      modalInstance.result.then(function (container) {
          containersService.delete(container.name).then(function(data) {
              var operationUrl = data.data.operation;
              var refreshInterval;

              // Try until operation is finished
              // refreshInterval = $interval(
              //     function() {
                      containersService.isOperationFinished(operationUrl).then(function(data) {
                        // $interval.cancel(refreshInterval);
                        // var index = $scope.containers.indexOf(container);
                        // $scope.containers.splice(index, 1);

                        containersService.getAll().then(function(data) {
                          $scope.containers = data;
                        });
                      });
                  //}
                  // , 500);

          });
      }, function () {
          // Nothing, user canceled
      });
  };

  })

  .controller('containerCreateCtrl', function ($scope, $routeParams, $filter, $location, $interval,
                                               containersService, profiles, images) {
      $scope.containerName = "";

      $scope.selectedImage = null;
      $scope.images = images;
      $scope.profiles = profiles;

      // $scope.profiles = profiles;
      $scope.selected = {
          name: '',
          image: '',
          ephemeral: false,
      }

      //
      // $scope.setImage = function (image) {
      //     $scope.selectedImage = image;
      // }

      $scope.createContainer = function (isValid) {
          $scope.isSubmitted = true;
          console.log($scope.selected);
          if (isValid) {
            // convert from selected profiles with full information
            // to an array
            // var profiles = [];
            // for(var n=0; n<$scope.selected.profile.length; n++) {
            //   profiles.push($scope.selected.profile[n].name);
            // }

              containersService.create(
                  $scope.selected.name,
                  $scope.selected.image.fingerprint,
                  [$scope.selected.profile.name],
                  $scope.selected.ephemeral)
                  .then(function(data) {
                    var operationUrl = data.data.operation;
                    // var refreshInterval;

                    // refreshInterval = $interval(
                    //     function() {

                            containersService.isOperationFinished(operationUrl).then(function(data) {
                                // $interval.cancel(refreshInterval);
                                window.location = "#!/containers";
                            });
                    //     },
                    //     500
                    // );
                });
          }

      }
  })

  .controller('containerViewCtrl', function ($scope, $routeParams, $interval, $uibModal,
                                             containerState, container, containersService) {
      $scope.container = container.data.metadata;
      $scope.container.state = containerState.data.metadata;

      $scope.changeState = function (container, state) {
          containersService.changeState(container.name, state).then(function(data) {
              var operationUrl = data.data.operation;
              // var refreshInterval;
              //
              // refreshInterval = $interval(
              //     function() {
                      $scope.container.state.disabled = true;
                      containersService.isOperationFinished(operationUrl).then(function(data) {
                        // $interval.cancel(refreshInterval);
                        if ((container.ephemeral == true) && (state == 'stop')) {
                          window.location = "#!/containers";
                        }
                        else {
                          containersService.getState(container.name).then(function(data) {
                              $scope.container.state = data.data.metadata;
                          });
                        }
                      });
              //     }, 500
              // );

          });
      }

      $scope.createImage = function(container) {
          var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'modules/containers/modalImageCreate.html',
              controller: 'containerImageCreateModalCtrl',
              size: 'sm',
              resolve: {
                  container: function () {
                      return container;
                  }
              }
          });

          modalInstance.result.then(function (imageData) {
              containersService.createImage(imageData.name,imageData.container.name).then(function (data) {
                  var operationUrl = data.data.operation;
              });
          }, function () {
              // Nothing
          });
      }

      $scope.update = function () {
          containersService.getState($scope.container.name).then(function(data) {
              $scope.container.state = data.data.metadata;
          });
      }

      $scope.delete = function (container) {

          var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'modules/containers/modalContainerDel.html',
              controller: 'genericContainerModalCtrl',
              size: 'sm',
              resolve: {
                  container: function () {
                      return container;
                  }
              }
          });

          modalInstance.result.then(function (container) {
              containersService.delete(container.name).then(function(data) {
                  var operationUrl = data.data.operation;
                  containersService.isOperationFinished(operationUrl).then(function(data) {
                    containersService.getAll().then(function(data) {
                      $scope.containers = data;
                      window.location = "#!/containers";
                    });
                  });
              });
          }, function () {
              // Nothing, user canceled
          });
      }
  })

  .controller('containerSnapshotCtrl', function ($scope, $routeParams, $interval, $filter, $location, $uibModal,
                                                 containersService, snapshots, container) {
      $scope.snapshots = snapshots;
      $scope.container = container.data.metadata;

      $scope.restore = function(snapshot) {
          var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'modules/containers/modalSnapshotRestore.html',
              controller: 'containerSnapshotRestoreModalCtrl',
              size: 'sm',
              resolve: {
                  container: function () {
                      return $scope.container;
                  },
                  snapshot: function() {
                      return snapshot;
                  }
              }
          });

          modalInstance.result.then(function (snapshotData) {
              containersService.restoreSnapshot($scope.container.name, snapshot.name).then(function(data) {

              });
          }, function () {
              // Nothing
          });
      }

      $scope.delete = function( snapshotData) {
          containersService.deleteSnapshot($scope.container.name, snapshotData).then(function (data) {
              var operationUrl = data.data.operation;
              // var refreshInterval;
              //
              // refreshInterval = $interval(
              //     function() {
                      containersService.isOperationFinished(operationUrl).then(function(data) {
                        // $interval.cancel(refreshInterval);
                        containersService.getSnapshots($scope.container.name).then(function(data) {
                            $scope.snapshots = data;
                        });
                      });
              //     }, 500
              // );
          });
      }
      //
      $scope.createSnapshot = function() {
          var modalInstance = $uibModal.open({
              animation: $scope.animationsEnabled,
              templateUrl: 'modules/containers/modalSnapshotCreate.html',
              controller: 'containerSnapshotCreateModalCtrl',
              size: 'sm',
              resolve: {
                  container: function () {
                      return $scope.container;
                  }
              }
          });

          modalInstance.result.then(function (snapshotData) {
              containersService.createSnapshot($scope.container.name, snapshotData).then(function (data) {
                  containersService.getSnapshots($scope.container.name).then(function(data) {
                      $scope.snapshots = data;
                  });
              });
          }, function () {
              // Nothing
          });

      }
  })

  .controller('containerSnapshotCreateModalCtrl', function ($scope, $routeParams, $filter, $location, $uibModalInstance,
                                                            container, containersService) {
      $scope.container = container;

      $scope.snapshotData = {
          name: "",
          stateful: false
      };

      $scope.ok = function () {
          $uibModalInstance.close($scope.snapshotData);
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  })

  .controller('containerSnapshotRestoreModalCtrl', function ($scope, $routeParams, $filter, $location, $uibModalInstance,
                                                             container, snapshot, containersService) {
      $scope.container = container;
      $scope.snapshot = snapshot;

      $scope.ok = function () {
          $uibModalInstance.close();
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  })

  .controller('containerImageCreateModalCtrl', function ($scope, $routeParams, $filter, $location, $uibModalInstance,
                                                            container, containersService) {
      $scope.imageData = {
          name: "",
          container: container
      };

      $scope.ok = function () {
          $uibModalInstance.close($scope.imageData);
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  })

  .controller('genericContainerModalCtrl', function ($scope, $routeParams, $filter, $location, $uibModalInstance,
                                                     container, containersService) {
      $scope.container = container;

      $scope.ok = function () {
          $uibModalInstance.close($scope.container);
      };

      $scope.cancel = function () {
          $uibModalInstance.dismiss('cancel');
      };
  })

  ;
