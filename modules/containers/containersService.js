'use strict';

angular.module('lxdAdmin.containers')
    .factory('containersService', ['$http', '$q', '$timeout','settings',
        function ($http, $q, $timeout, settings) {
            var obj = {};

            obj.isOperationFinished = function (operationUrl) {
                return $http.get(settings.apiUrl + operationUrl + '/wait');
            };

            obj.getByName = function (containerName) {
                return $http.get(settings.apiUrl + settings.version + '/containers/' + containerName).then(function(response) {
                  return response;
                });
            };

            // Get all containers,  including detailed data
            obj.getAll = function () {
                // Sync
                return $http.get(settings.apiUrl + settings.version + '/containers').then(function (data) {
                    data = data.data;

                    if (data.status != "Success") {
                        return $q.reject("Error");
                    }

                    var promises = data.metadata.map(function (containerUrl) {
                        return $http.get(settings.apiUrl + containerUrl).then(function (resp) {
                            return resp.data.metadata;
                        });
                    });

                    return $q.all(promises);
                });
            };


            // Get all containers,  including detailed data
            // obj.getAllSimple = function () {
            //     // Sync
            //     return $http.get(SettingServices.getLxdApiUrl() + '/containers');
            // };
            //
            //
            // // Create container:
            obj.create = function (containerName, imageFingerprint, profiles, ephemeralState) {
                var containerData = {
                  name: containerName,
                  profiles: profiles,
                  ephemeral: ephemeralState,
                  source: {
                      type: "image",
                      fingerprint: imageFingerprint,
                  }
                };
                // Async
                return $http.post(settings.apiUrl + settings.version + '/containers', containerData)
            };
            //
            obj.createImage = function (imageName,containerName) {
              //  console.log(imageName,containerName);
                var imageData = {
                	properties: {
                        aliases:imageName,
                        description:imageName + " (" + containerName + ")"
                    },
                    source: {
                        type: "container",
                        name: containerName
                    }
                };
                // Async
                return $http.post(settings.apiUrl + settings.version + '/images', imageData)
            };
            //
            // obj.createFromAlias = function (containerData, imageData) {
            //     containerData.source = {
            //         type: "image",
            //         alias: "ubuntu/16.04"
            //     };
            //     return $http.post(SettingServices.getLxdApiUrl() + '/containers', containerData);
            // };
            //
            //
            // // Modify container:
            // obj.modify = function (containerName, containerData) {
            //     delete containerData['name'];
            //     delete containerData['status'];
            //
            //     return $http.put(SettingServices.getLxdApiUrl() + '/containers/' + containerName, containerData).then(function (data) {
            //         return(data);
            //     });
            // };
            //
            //
            // // Rename  a container
            // // Mode: Async
            // obj.rename = function (containerName, containerData) {
            //     return $http.post(SettingServices.getLxdApiUrl() + '/containers/' + containerName, containerData);
            // };
            //
            //
            // // Delete a container
            // // Mode: Async
            obj.delete = function (containerName) {
                return $http.delete(settings.apiUrl + settings.version + '/containers/' + containerName).then(function (data) {
                    return data;
                });
            };
            //
            //
            // /** State **/
            obj.getState = function (containerName) {
                return $http.get(settings.apiUrl + settings.version + '/containers/' + containerName + '/state');
            };
            //
            //
            obj.changeState = function (containerName, state) {
                var data =
                {
                    "action": state,        // State change action (stop, start, restart, freeze or unfreeze)
                    "timeout": 30,          // A timeout after which the state change is considered as failed
                    "force": true,          // Force the state change (currently only valid for stop and restart where it means killing the container)
                    "stateful": false        // Whether to store or restore runtime state before stopping or startiong (only valid for stop and start, defaults to false)
                };

                return $http.put(settings.apiUrl + settings.version + '/containers/' + containerName + '/state', data);
            };
            //
            //
            // /** Snapshot **/
            //
            obj.getSnapshots = function(containerName) {
              return $http.get(settings.apiUrl + settings.version + '/containers/' + containerName + '/snapshots').then(function (data) {
                  data = data.data;

                  if (data.status != "Success") {
                      return $q.reject("Error");
                  }


                  var promises = data.metadata.map(function (containerUrl) {
                      return $http.get(settings.apiUrl + containerUrl).then(function (resp) {
                          return resp.data.metadata;
                      });
                  });

                  return $q.all(promises);
              });

            };
            //
            //
            obj.createSnapshot = function(containerName, snapshotData) {
              return $http.post(settings.apiUrl + settings.version + '/containers/' + containerName + '/snapshots', snapshotData);
            };
            //
            //
            obj.restoreSnapshot = function(containerName, snapshotName) {
              var data = {
                restore: snapshotName,
              };
              return $http.put(settings.apiUrl + settings.version + '/containers/' + containerName, data);
            };

            obj.deleteSnapshot = function(containerName, snapshotName) {
              var snapshotName = snapshotName.name.split('/')[1]
              return $http.delete(settings.apiUrl + settings.version + '/containers/' + containerName + "/snapshots/" + snapshotName);
            };

            return obj;
        }])
;
