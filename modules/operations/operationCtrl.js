'use strict';

angular.module('lxdAdmin.operation', ['ngRoute'])
    // .config(['$routeProvider', function ($routeProvider) {
    //     $routeProvider.when('/operations', {
    //         title: 'Operations',
    //         templateUrl: 'modules/operations/operations.html',
    //         controller: 'operationsListCtrl'
    //         // resolve: {
    //         //     operations: function (operationService, $route) {
    //         //         return operationService.getAll();
    //         //     }
    //         // }
    //     })
    //     // .when('/operation-view/:operationID', {
    //     //     title: 'Operations',
    //     //     templateUrl: 'modules/operation/operation-view.html',
    //     //     controller: 'operationViewCtrl',
    //     //     resolve: {
    //     //         operation: function (OperationServices, $route) {
    //     //             return OperationServices.getByFingerprint($route.current.params.operationID)
    //     //         }
    //     //     }
    //     // })
    //     ;
    // }])


    // .controller('operationHeaderCtrl', function ($scope, $routeParams, $filter, $location, $interval,
    //   OperationServices) {
    //     $scope.refresh = function() {
    //       OperationServices.getAll().then(function(data) {
    //         var res = [];
    //
    //         if (data instanceof Array && ! _.isEmpty(data)) {
    //           data.forEach(function(d) {
    //             if (d.class != "websocket") {
    //               res.push(d);
    //             }
    //           });
    //         }
    //
    //         $scope.operations = res;
    //       });
    //     }
    // })
    //
    //
    // .controller('operationViewCtrl', function ($scope, $routeParams, $filter, $location, operation, OperationServices) {
    //     $scope.operation = operation.data.metadata;
    // })

    .controller('operationListCtrl', function ($scope, $routeParams, $filter, $interval,
                                           operationService) {

        $scope.operations = [];

        $interval(function () {
          operationService.getAll().then(function(operations){
            if (operations) {
              if ($scope.operations.length != operations.length) {
                $scope.showOperations = true;
                // console.log(true);
                $scope.operations = [];
                operations.forEach(function(operation){
                  if (operation.class != 'websocket')
                    $scope.operations.push(operation);
                });
              }

            }
          })
        }, 5000);

    })
;
